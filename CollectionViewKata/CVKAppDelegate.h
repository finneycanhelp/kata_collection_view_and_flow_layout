//
//  CVKAppDelegate.h
//  CollectionViewKata
//
//  Created by Michael Finney on 8/27/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
