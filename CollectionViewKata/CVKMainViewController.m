
#import "CVKMainViewController.h"
#import "CVKRecipeCollectionViewCell.h"

@interface CVKMainViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property NSArray *recipeImages;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@end

@implementation CVKMainViewController

- (void)viewDidLayoutSubviews;
{
    // htt p://stackoverflow.com/questions/2824435/uiscrollview-not-scrolling
    // "..can set contentSize in viewDidLayoutSubviews:, and it will be applied after the autolayout completes"
    [super viewDidLayoutSubviews];

    
    
    CGFloat height = 600; // TODO: change to be the height of the tallest cell / cell content view
    self.scrollView.contentSize = CGSizeMake(0, height);
    
//    self.scrollView.contentSize = CGSizeMake(568, height);
}


- (void)viewDidLoad;
{
    [super viewDidLoad];
    
    self.recipeImages = [NSArray arrayWithObjects:@"angry_birds_cake.jpg",
                         @"creme_brelee.jpg", @"egg_benedict.jpg", @"full_breakfast.jpg", @"green_tea.jpg", @"ham_and_cheese_panini.jpg", @"ham_and_egg_sandwich.jpg", @"hamburger.jpg", @"instant_noodle_with_egg.jpg", @"japanese_noodle_with_pork.jpg", @"mushroom_risotto.jpg", @"noodle_with_bbq_pork.jpg", @"starbucks_coffee.jpg", @"thai_shrimp_cake.jpg", @"vegetable_curry.jpg", @"white_chocolate_donut.jpg", nil];
    
    [self changeCellItemSize];
}

- (void)changeCellItemSize
{
    CGSize itemSize = self.flowLayout.itemSize;
    itemSize.height = 600;
    self.flowLayout.itemSize = itemSize;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return self.recipeImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *identifier = @"Cell";
    
    CVKRecipeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.imageView.image = [UIImage imageNamed:self.recipeImages[indexPath.row]];

//    [cell sizeToFit]; could be cool if needed
    
    return cell;
}

@end
