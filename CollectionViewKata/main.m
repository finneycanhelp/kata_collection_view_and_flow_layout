//
//  main.m
//  CollectionViewKata
//
//  Created by Michael Finney on 8/27/14.
//  Copyright (c) 2014 Mike Finney. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CVKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CVKAppDelegate class]));
    }
}
